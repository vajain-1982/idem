import pathlib
import subprocess
import sys
import tempfile

import aio_pika
import aiokafka.structs
import kafka.errors
import pytest
import yaml

KAFKA_PROFILE_NAME = "test_development_evbus_kafka"
RABBITMQ_PROFILE_NAME = "test_development_evbus_pika"
ROUTING_KEY = "key"


@pytest.fixture
def acct_key(hub):
    if not hub.OPT.acct.acct_key:
        raise pytest.skip("No ACCT_KEY environment variable found for this test")
    yield hub.OPT.acct.acct_key


@pytest.fixture
async def kafka_ctx(hub, acct_key):
    if not hub.OPT.acct.acct_file:
        raise pytest.skip("No ACCT_FILE environment variable found for this test")

    profiles = await hub.acct.init.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=acct_key,
    )
    ctx = profiles.get("kafka", {}).get(KAFKA_PROFILE_NAME)
    if not ctx:
        raise pytest.skip(
            f"No profile named '{KAFKA_PROFILE_NAME}' in '{hub.OPT.acct.acct_file}'"
        )
    yield ctx


@pytest.fixture(name="kafka")
async def kafka_consumer(hub, kafka_ctx):
    """
    Start a local kafka server to run this test:
    .. code-block:: bash

        $ docker run -p 2181:2181 -p 443:9092 -p 9092:9092 \
          --env ADVERTISED_LISTENERS=PLAINTEXT://localhost:443,INTERNAL://localhost:9093 \
          --env LISTENERS=PLAINTEXT://0.0.0.0:9092,INTERNAL://0.0.0.0:9093 \
          --env SECURITY_PROTOCOL_MAP=PLAINTEXT:PLAINTEXT,INTERNAL:PLAINTEXT \
          --env INTER_BROKER=INTERNAL \
          krisgeus/docker-kafka

    Credentials for connecting:
    .. code-block:: sls

        kafka:
          test_development_evbus_kafka:
            connection:
              bootstrap_servers: localhost:9092
    """
    try:
        async with aiokafka.AIOKafkaConsumer(
            ROUTING_KEY, **kafka_ctx["connection"]
        ) as consumer:
            # Start with a clean queue
            await consumer.getmany(timeout_ms=0)
            yield consumer
    except kafka.errors.KafkaConnectionError as e:
        raise pytest.skip(f"Could not connect to kafka server: {e}")


@pytest.fixture
async def pika_ctx(hub):
    if not (hub.OPT.acct.acct_file and hub.OPT.acct.acct_key):
        raise pytest.skip(
            "No ACCT_KEY and ACCT_FILE environment variables found for this test"
        )

    profiles = await hub.acct.init.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=hub.OPT.acct.acct_key,
    )
    for provider in ("pika", "amqp", "rabbitmq"):
        ctx = profiles.get(provider, {}).get(RABBITMQ_PROFILE_NAME)
        if ctx:
            break
    else:
        raise pytest.skip(
            f"No profile named '{RABBITMQ_PROFILE_NAME}' in '{hub.OPT.acct.acct_file}'"
        )
    yield ctx


@pytest.fixture
async def rabbitmq(hub, event_loop, pika_ctx):
    """
    Start a local rabbitmq server to run this test:
    .. code-block:: bash

        $ docker run -p 5672:5672 \
          --env RABBITMQ_HOSTS=rabbitmq \
          --env RABBITMQ_PORT=5672 \
          --env RABBITMQ_USER=guest \
          --env RABBITMQ_PASS=guest \
          --env RABBITMQ_PROTOCOL=amqp \
          rabbitmq:management

    Credentials for connecting:
    .. code-block:: sls

        pika:
          test_development_evbus_pika:
            connection:
              host: localhost
              port: 5672
              login: guest
              password: guest
    """
    try:
        conn: aio_pika.RobustConnection = await aio_pika.connect(
            loop=event_loop, **pika_ctx["connection"]
        )
    except ConnectionError as e:
        raise pytest.skip(f"Could not connect to rabbitmq server\n{e}")

    async with conn.channel() as channel:
        queue: aio_pika.RobustQueue = await channel.declare_queue(name=ROUTING_KEY)
        # Start with a clean queue
        await queue.purge()
        yield queue

        await queue.delete(if_unused=False, if_empty=False)
        await conn.close()


@pytest.fixture
def event_acct_file(hub, pika_ctx, kafka_ctx, acct_key, runpy, request):
    """
    Create a ctx that can be used to get all idem events based on the test profiles
    """
    # Get the event_profile_name from the parametrization I.E.
    #   @pytest.mark.parametrize("event_acct_file", ["<profile_name>"], indirect=True)
    event_profile_name = request.param

    with tempfile.NamedTemporaryFile(mode="w+", suffix=".sls") as fh:
        profiles = {
            "pika": [{event_profile_name: pika_ctx}],
            "kafka": {event_profile_name: kafka_ctx},
        }
        yaml.safe_dump(profiles, fh)
        fh.flush()

        proc = subprocess.Popen(
            [sys.executable, runpy, "encrypt", fh.name, f'--acct-key="{acct_key}"'],
            env={},
        )
        assert proc.wait() == 0
        new_acct_file = pathlib.Path(f"{fh.name}.fernet")
        assert new_acct_file.exists()
        yield new_acct_file
        new_acct_file.unlink()
