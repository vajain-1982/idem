from tests.integration.conftest import run_sls


def test_include(hub):
    ret = run_sls(["include"])
    assert ret
    assert len(ret) == 5, "Expecting 5 states"
    for state, ret in ret.items():
        assert ret["result"] is True, state


def test_include_missing_files(hub):
    # TODO we get the same error twice
    ret = run_sls(["invalid_include"])
    assert "SLS ref 'dir.non_existing_file' did not resolve to a file" in ret
