import os
import pathlib
import tempfile

import dict_tools.data
import pytest


@pytest.fixture(name="ctx")
def local_ctx():
    return dict_tools.data.NamespaceDict(
        run_name="test", cache_dir=pathlib.Path(tempfile.gettempdir())
    )


async def test_context_existing(hub):
    """
    With a nested context, the pid will already exist when the second context runs
    """
    async with hub.idem.managed.context("test", tempfile.gettempdir()):
        with pytest.raises(ChildProcessError):
            async with hub.idem.managed.context("test", tempfile.gettempdir()):
                ...


async def test_different_context(hub):
    """
    With different run_names,
    """
    async with hub.idem.managed.context("test", tempfile.gettempdir()):
        async with hub.idem.managed.context("different", tempfile.gettempdir()):
            ...


async def test_context(hub, ctx):
    """
    Verify that the pid_file exists in the context with an integer
    """
    path: pathlib.Path = await hub.esm.local.enter(ctx)
    try:
        with path.open("r") as fh:
            assert int(fh.read()) == os.getpid()
    finally:
        path.unlink()


async def test_verify_exists(hub):
    pid_file = pathlib.Path("non-existent")
    assert await hub.esm.local.verify(pid_file) is None
    assert not pid_file.exists()


async def test_verify_contents_valid(hub):
    with tempfile.NamedTemporaryFile(suffix=".pid", mode="w+") as fh:
        fh.write(str(os.getpid()))
        fh.flush()

        pid_file = pathlib.Path(fh.name)

        assert await hub.esm.local.verify(pid_file) == os.getpid()
        assert pid_file.exists()

    assert not pid_file.exists()


async def test_verify_contents_invalid(hub):
    with tempfile.NamedTemporaryFile(suffix=".pid", mode="w+") as fh:
        fh.write("crash")
        fh.flush()

        pid_file = pathlib.Path(fh.name)

        assert await hub.esm.local.verify(pid_file) is None
        assert pid_file.exists()

    assert not pid_file.exists()


async def test_set_state(hub, ctx):
    await hub.esm.local.set_state(ctx, {"key": "value"})
    ret = await hub.esm.local.get_state(ctx)
    assert ret == {"key": "value"}
