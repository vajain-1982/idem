import json
import pathlib
import subprocess
import sys
import tempfile

import pytest

from .conftest import ROUTING_KEY


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-exec", "idem-*"], indirect=True)
async def test_exec(hub, runpy, kafka, rabbitmq, event_acct_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "exec",
            "test.ping",
            f"--run-name={ROUTING_KEY}",
            f"--acct-file={event_acct_file}",
        ],
    )
    assert proc.wait() == 0

    expected = {
        "message": {"result": True, "ret": True},
        "tags": {"ref": "exec.test.ping", "type": "exec-post"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-low"], indirect=True)
async def test_low(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": [
            {
                "__id__": "s",
                "__sls__": path.stem,
                "fun": "succeed_without_changes",
                "name": "s",
                "order": 100000,
                "state": "test",
            }
        ],
        "tags": {"ref": "idem.run.init.start", "type": "state-low-data"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-high"], indirect=True)
async def test_high(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "s": {
                "__sls__": path.stem,
                "test": ["succeed_without_changes", {"order": 100000}],
            }
        },
        "tags": {"ref": "idem.resolve.introduce", "type": "state-high-data"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-status"], indirect=True)
async def test_status(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

    await hub.pop.loop.sleep(2)

    for status in ("GATHERING", "COMPILING", "RUNNING", "FINISHED"):
        expected = {
            "message": status,
            "tags": {"ref": "idem.state.update_status", "type": "state-status"},
        }

        # Test rabbitmq
        received_message = await rabbitmq.get()
        assert json.loads(received_message.body) == expected

        # Test Kafka
        received_message = await kafka.getone()
        assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-run"], indirect=True)
async def test_run(hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

        sls_path = pathlib.Path(fh.name).parent

    expected = {
        "message": {
            "acct_profile": "default",
            "add_low": [],
            "blocks": {},
            "cache_dir": hub.OPT.idem.cache_dir,
            "errors": [],
            "files": [],
            "high": {},
            "iorder": 100000,
            "meta": {"ID_DECS": {}, "SLS": {}},
            "param_sources": [],
            "params": {},
            "params_processing": False,
            "post_low": [],
            "render": "jinja|yaml",
            "resolved": [],
            "run_num": 1,
            "running": {},
            "runtime": "parallel",
            "sls_refs": {},
            "sls_sources": [f"file://{sls_path}"],
            "states": {},
            "status": 1,
            "subs": ["states"],
            "test": False,
            "managed_state": ["test_|-s_|-s_|-succeed_without_changes"],
        },
        "tags": {"ref": "idem.state.create", "type": "state-run"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-chunk"], indirect=True)
async def test_chunk(hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_with_comment:\n    comment: asdf")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "__id__": "s",
            "__sls__": path.stem,
            "fun": "comment",
            "name": "s",
            "order": 1,
            "state": "test.succeed_with_comment",
        },
        "tags": {"ref": "idem.rules.init.run", "type": "state-chunk"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_pre_post(
    hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx
):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--run-name={ROUTING_KEY}",
                f"--acct-file={event_acct_file}",
            ],
        )
        assert proc.wait() == 0

    pathlib.Path(fh.name)

    expected_pre = {
        "message": {
            "s": {
                "test.succeed_without_changes": {
                    "ctx": {"old_state": None, "run_name": "key", "test": False},
                    "kwargs": {},
                    "name": "s",
                }
            }
        },
        "tags": {"ref": "states.test.succeed_without_changes", "type": "state-pre"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {},
            "comment": "Success!",
            "name": "s",
            "result": True,
        },
        "tags": {"ref": "states.test.succeed_without_changes", "type": "state-post"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post
