State {{ params.get('state').get('name') }} Present:
  nest.test.succeed_with_kwargs_as_changes:
    - name: {{ params.get('state').get('name') }}
    - parameters:
        location: {{ params['locations'][0] }}
        backup_location: {{ params['locations'][1] }}
