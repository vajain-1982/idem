.. idem documentation master file, created by
   sphinx-quickstart on Wed Feb 20 15:36:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem's documentation!
================================

.. toctree::
   :maxdepth: 3
   :glob:

   topics/intro
   topics/extending
   topics/add_requisites
   topics/sls_meta
   topics/sls_structure
   topics/parameters
   topics/argument_binding
   topics/jmespath
   topics/transparent_req
   topics/acct
   topics/sls_tree
   topics/sls_resolver_plugins
   topics/reconciliation_loop
   topics/enforced_state_management
   topics/events
   topics/helper_functions/index
   tutorial/index

   topics/azure_docs
   topics/migrate_salt
   releases/index

.. toctree::
   :caption: Get involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
