META:
  foo: Bar
  baz:
    - one
    - 1
    - True
  bar:
    another: 4

happy:
  META:
    deep: id_space
  test.nop: []

sad:
  test.fail_without_changes

ahappy:
  test.anop
