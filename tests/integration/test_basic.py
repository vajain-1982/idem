import pytest
import rend.exc

from tests.integration.conftest import run_sls


def test_meta():
    ret = run_sls(["meta"], ret_data="all")
    assert "meta" in ret
    assert ret["meta"]["SLS"]["meta"]["foo"] == "Bar"
    assert ret["meta"]["SLS"]["meta"]["baz"] == ["one", 1, True]
    assert ret["meta"]["SLS"]["meta"]["bar"] == {"another": 4}
    assert ret["meta"]["ID_DECS"]["meta.happy"]["deep"] == "id_space"


def test_treq():
    ret = run_sls(["treq"])
    assert ret["test_|-to_treq_|-to_treq_|-treq"]["__run_num"] == 4


def test_ugly1():
    ret = run_sls(["ugly1"])
    assert ret == ["ID foo in SLS ugly1 is not a dictionary"]


def test_shebang():
    ret = run_sls(["bang"])
    assert "test_|-test_|-test_|-nop" in ret


def test_req_chain():
    """
    Test that you can chain requisites, bug #11
    """
    ret = run_sls(["recreq"])
    assert ret.get("test_|-first thing_|-first thing_|-nop", {}).get("__run_num") == 1
    assert ret.get("test_|-second thing_|-second thing_|-nop", {}).get("__run_num") == 2
    assert ret.get("test_|-third thing_|-third thing_|-nop", {}).get("__run_num") == 3


def test_nest():
    ret = run_sls(["nest"])
    assert ret["nest.again.another.test_|-baz_|-baz_|-nop"]["result"]
    assert ret["nest.again.test_|-bar_|-bar_|-nop"]["result"]
    assert ret["nest.test_|-foo_|-foo_|-nop"]["result"]
    # verify that the invalid state is not run
    assert not ret["idem.init_|-quo_|-quo_|-create"]["result"]
    assert ret["test_|-req_|-req_|-nop"]["__run_num"] == 5


def test_basic():
    """
    Test the basic funcitonality of Idem
    """
    ret = run_sls(["simple"])
    assert ret["test_|-happy_|-happy_|-nop"]["result"] == True
    assert ret["test_|-happy_|-happy_|-nop"]["changes"] == {}
    assert ret["test_|-happy_|-happy_|-nop"]["name"] == "happy"
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["result"] == False
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["name"] == "sad"
    assert ret["test_|-sad_|-sad_|-fail_without_changes"]["changes"] == {}


def test_any():
    """
    Test the require_any funcitonality of Idem
    """
    ret = run_sls(["any"])
    assert ret["test_|-bad_|-bad_|-nop"]["result"] == False
    assert ret["test_|-runs_|-runs_|-nop"]["result"] == True


def test_req():
    """
    Test basic requisites
    """
    ret = run_sls(["req"])
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["result"] == False
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["__run_num"] == 4
    assert ret["test_|-needs_|-needs_|-nop"]["__run_num"] == 3
    assert ret["test_|-needs_|-needs_|-nop"]["result"] == True


def test_prereq():
    """
    Test prereq
    """
    ret = run_sls(["prereq"])
    assert ret["test_|-pre_|-pre_|-nop"]["result"] == True
    assert ret["test_|-pre_|-pre_|-nop"]["__run_num"] == 2
    assert ret["test_|-good_|-good_|-nop"]["__run_num"] == 1
    assert ret["test_|-change_1_|-change_1_|-succeed_with_changes"]["__run_num"] == 3
    assert ret["test_|-change_1_|-change_1_|-succeed_with_changes"]["changes"]


def test_mod_aggregate():
    """
    Test prereq
    """
    ret = run_sls(["agg"])
    assert (
        ret["nest.agg_|-aggregate_|-aggregate_|-comment"]["comment"]
        == "Modified by mod_aggregate and the mod system"
    )


def test_req_test_mode():
    """
    Test basic requisites in test mode
    """
    ret = run_sls(["req"], test=True)
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["result"] == False
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["__run_num"] == 4
    assert ret["test_|-needs_|-needs_|-nop"]["__run_num"] == 3
    # "needed" returned None and needs did not fail to run
    assert ret["test_|-needs_|-needs_|-nop"]["result"] == None


def test_watch():
    """
    Test basic requisites
    """
    ret = run_sls(["watch"])
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["__run_num"] == 2
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["comment"] == "Watch ran!"
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["result"] == True
    assert ret["test_|-changes_|-changes_|-succeed_with_changes"]["result"] == True
    assert ret["test_|-changes_|-changes_|-succeed_with_changes"]["changes"]


def test_listen():
    """
    Test basic requisites
    """
    ret = run_sls(["listen"])
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"][
            "__run_num"
        ]
        == 3
    )
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"][
            "comment"
        ]
        == "Watch ran!"
    )
    assert (
        ret["test_|-listen_changes_listen_|-listen_changes_listen_|-mod_watch"]["name"]
        == "listen_changes_listen"
    )


def test_onfail():
    """
    Test basic requisites
    """
    ret = run_sls(["fails"])
    assert ret["test_|-runs_|-runs_|-nop"]["__run_num"] == 2
    assert ret["test_|-runs_|-runs_|-nop"]["result"] == True
    assert ret["test_|-bad_|-bad_|-nop"]["result"] == False
    assert ret["test_|-bad_|-bad_|-nop"]["__run_num"] == 3
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["__run_num"] == 1
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["result"] == False


def test_onchanges():
    ret = run_sls(["changes"])
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["__run_num"] == 2
    assert ret["test_|-watch_changes_|-watch_changes_|-nop"]["result"] == True


def test_run_name():
    ret = run_sls(["update"])
    assert ret["test_|-king_arthur_|-totally_extra_alls_|-nop"]["__run_num"] == 2


def test_params():
    ret = run_sls(["order"], runtime="serial")
    assert ret["test_|-first_|-first_|-noop"]["__run_num"] == 1
    assert ret["test_|-second_|-second_|-noop"]["__run_num"] == 2
    assert ret["test_|-third_|-third_|-noop"]["__run_num"] == 3
    assert ret["test_|-forth_|-forth_|-noop"]["__run_num"] == 4
    assert ret["test_|-fifth_|-fifth_|-noop"]["__run_num"] == 5
    assert ret["test_|-sixth_|-sixth_|-noop"]["__run_num"] == 6
    assert ret["test_|-seventh_|-seventh_|-noop"]["__run_num"] == 7
    assert ret["test_|-eighth_|-eighth_|-noop"]["__run_num"] == 8
    assert ret["test_|-ninth_|-ninth_|-noop"]["__run_num"] == 9
    assert ret["test_|-tenth_|-tenth_|-noop"]["__run_num"] == 10


def test_blocks():
    ret = run_sls(["blocks"])
    assert "test_|-wow_|-wow_|-nop" in ret


def test_dup_keys():
    with pytest.raises(rend.exc.RenderException):
        run_sls(["dupkeys"])


def test_acct(code_dir):
    acct_fn = code_dir.joinpath("tests", "files", "acct.fernet")
    acct_key = "eWO2UroAYY3Dff8uKcT32iiBHW2qVkVaDV3vIQoIaJU="
    ret = run_sls(["acct"], acct_file=acct_fn, acct_key=acct_key)
    assert (
        ret["nest.acct_|-gather_acct_|-gather_acct_|-gather"]["comment"]
        == '{"foo": "bar"}'
    )
    assert (
        ret["nest.acct_|-another_acct_|-another_acct_|-gather"]["comment"]
        == '{"quo": "qux"}'
    )


def test_jinja_exec_ctx():
    ret = run_sls(["jinctx"], acct_file=None, acct_key=None)
    assert ret == {
        "test_|-test_ctx_explicit_full_path_|-test_ctx_explicit_full_path_|-succeed_with_comment": {
            "__run_num": 3,
            "changes": {},
            "comment": {"acct": {}},
            "name": "test_ctx_explicit_full_path",
            "result": True,
        },
        "test_|-test_ctx_minimal_|-test_ctx_minimal_|-succeed_with_comment": {
            "__run_num": 2,
            "changes": {},
            "comment": {"acct": {}},
            "name": "test_ctx_minimal",
            "result": True,
        },
        "test_|-test_implicit_ctx_|-test_implicit_ctx_|-succeed_with_comment": {
            "__run_num": 1,
            "changes": {},
            "comment": {"acct": {}},
            "name": "test_implicit_ctx",
            "result": True,
        },
        "test_|-test_minimal_ctx_|-test_minimal_ctx_|-succeed_with_comment": {
            "__run_num": 4,
            "changes": {},
            "comment": {"acct": {}},
            "name": "test_minimal_ctx",
            "result": True,
        },
    }


def test_arg_bind():
    """
    Test that you can do arg_binding
    """
    ret = run_sls(["arg_bind"])
    assert ret["test_|-arg bind_|-arg bind_|-succeed_with_arg_bind"]["result"] == True
    changes = ret.get("test_|-arg bind_|-arg bind_|-succeed_with_arg_bind", {}).get(
        "changes", {}
    )
    assert (
        changes.get("testing", {}).get("test1", None) == "Something pretended to change"
    )
    assert (
        changes.get("testing", {}).get("test2", None) == "Something pretended to change"
    )

    assert (
        ret["test_|-indexed arg_|-indexed arg_|-succeed_with_arg_bind"]["result"]
        == True
    )
    changes = ret.get(
        "test_|-indexed arg_|-indexed arg_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert changes.get("testing", {}).get("tests", [])[1].get("test", {}) == "new_test"

    assert (
        ret["test_|-fail no new_state_|-fail no new_state_|-succeed_with_arg_bind"][
            "result"
        ]
        == False
    )
    assert (
        ret["test_|-fail no new_state_|-fail no new_state_|-succeed_with_arg_bind"][
            "comment"
        ]
        == '"test:second thing" state does not have "new_state" in the state returns.'
    )

    assert (
        ret["test_|-fail arg not found_|-fail arg not found_|-succeed_with_arg_bind"][
            "result"
        ]
        == False
    )
    assert (
        ret["test_|-fail arg not found_|-fail arg not found_|-succeed_with_arg_bind"][
            "comment"
        ]
        == '"testing:arg_not_found" is not found as part of "test" state "new_state".'
    )

    assert (
        ret[
            "test_|-fail referenced arg index not found_|-fail referenced arg index not found_|-succeed_with_arg_bind"
        ]["result"]
        == False
    )
    assert (
        ret[
            "test_|-fail referenced arg index not found_|-fail referenced arg index not found_|-succeed_with_arg_bind"
        ]["comment"]
        == 'Cannot parse argument key tests[0][5]:new for index "5", because argument key is not a list or it does not include element with index "5".'
    )

    assert (
        ret[
            "test_|-fail arg index not found_|-fail arg index not found_|-succeed_with_arg_bind"
        ]["result"]
        == False
    )
    assert (
        ret[
            "test_|-fail arg index not found_|-fail arg index not found_|-succeed_with_arg_bind"
        ]["comment"]
        == 'Cannot parse argument key test for index "1", because argument key is not a list or it does not include element with index "1".'
    )


def test_arg_bind_ref():
    """
    Test that you can do argument binding via references
    """
    ret = run_sls(["arg_bind_ref"])
    assert (
        ret["test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind"]["result"]
        == True
    )
    changes = ret.get(
        "test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert (
        changes.get("testing", {}).get("test1", None)
        == "First - Something pretended to change. Second - new_test. Finished"
    )
    assert changes.get("testing", {}).get("test2", None) == "new_test"
    assert (
        changes.get("testing", {}).get("test3", [])[0]
        == "Something pretended to change"
    )
    assert changes.get("testing", {}).get("test3", [])[1] == "new_test -- new_test"

    assert (
        ret[
            "test_|-reference within list_|-reference within list_|-succeed_with_arg_bind"
        ]["result"]
        == True
    ), ret["comment"]
    """
    assert (
        ret[
            "test_|-fail reference format_|-fail reference format_|-succeed_with_arg_bind"
        ]["comment"]
        == 'Cannot set argument value for index "1", because "test" is not a list or it does not include element with index "1".'
    )
    """
