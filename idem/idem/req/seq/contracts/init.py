from typing import Any
from typing import Dict


def sig_run(
    hub, seq: Dict[int, Dict[str, Any]], low: Dict[str, Any], running: Dict[str, Any]
) -> Dict[int, Dict[str, Any]]:
    ...
