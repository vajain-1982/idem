import pathlib
import tempfile

import pop.hub


def run_sls_validate(
    sls,
    runtime="parallel",
    test=False,
    acct_file=None,
    acct_key=None,
    ret_data="running",
):
    """
    Pass in an sls list and run it!
    """
    name = "test"
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add("tests.nest")
    hub.pop.sub.load_subdirs(hub.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest.again)
    render = "jinja|yaml"
    cache_dir = tempfile.mkdtemp()
    sls_dir = pathlib.Path(__file__).parent.parent / "sls/validate"
    sls_sources = [f"file://{sls_dir}"]
    hub.pop.loop.create()
    hub.pop.Loop.run_until_complete(
        hub.idem.state.validate(
            name,
            sls_sources,
            render,
            runtime,
            ["states", "nest"],
            cache_dir,
            sls,
            test,
        )
    )
    errors = hub.idem.RUNS[name]["errors"]
    if errors:
        return errors
    if ret_data == "all":
        return hub.idem.RUNS[name]
    return hub.idem.RUNS[name]["params"]


def test_validate_params(hub):
    ret = run_sls_validate(["success"])
    assert ret
    params = ret.params()
    assert params
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""
    assert params["rg_name"]["your_rg"] == "default"
    assert params["locations"]
    assert isinstance(params["locations"], list)
    assert len(params["locations"]) == 1
    assert isinstance(params["locations"][0]["state"], dict)
    assert params["locations"][0]["state"]["city"] == ""
