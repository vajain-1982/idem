import pytest
import rend.exc

from tests.integration.conftest import run_sls


def test_params(hub):
    ret = run_sls(["success"], params="params", sls_offset="sls/params")
    assert ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["changes"]
        assert ret["changes"]["parameters"]
        assert ret["changes"]["parameters"]["location"] == "eastus"
        assert ret["changes"]["parameters"]["backup_location"] == "westus"


def test_params_failure(hub):
    with pytest.raises(rend.exc.RenderException):
        run_sls(["failure"], params="params", sls_offset="sls/params")
