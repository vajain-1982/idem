import pathlib
import tempfile
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(
            ["idem", "rend", "acct", "evbus"], cli="idem", parse_cli=False
        )
    hub.serialize.PLUGIN = "json"
    yield hub


@pytest.fixture(scope="module")
def acct_key():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="crypto")
    yield hub.crypto.fernet.generate_key()


@pytest.fixture(scope="session")
def runpy():
    yield pathlib.Path(__file__).parent.parent.parent / "run.py"


def run_sls(
    sls,
    runtime="parallel",
    test=False,
    acct_file=None,
    acct_key=None,
    ret_data="running",
    sls_offset=None,
    params=None,
):
    """
    Pass in an sls list and run it!
    """
    name = "test"
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.sub.add("tests.nest")
    hub.pop.sub.load_subdirs(hub.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest)
    hub.pop.sub.load_subdirs(hub.nest.nest.again)
    render = "jinja|yaml"
    cache_dir = tempfile.mkdtemp()
    if sls_offset is None:
        sls_offset = "sls"
    sls_dir = pathlib.Path(__file__).parent.parent / sls_offset
    param_sources = sls_sources = [f"file://{sls_dir}"]
    hub.pop.loop.create()
    hub.pop.Loop.run_until_complete(
        hub.idem.state.apply(
            name,
            sls_sources,
            render,
            runtime,
            ["states", "nest"],
            cache_dir,
            sls,
            test,
            acct_file,
            acct_key,
            managed_state={},
            param_sources=param_sources,
            params_file=params,
        )
    )
    errors = hub.idem.RUNS[name]["errors"]
    if errors:
        return errors
    if ret_data == "all":
        return hub.idem.RUNS[name]
    return hub.idem.RUNS[name]["running"]
