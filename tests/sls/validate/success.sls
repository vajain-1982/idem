
Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}:
  azure.resource_management.resource_groups.present:
    - resource_group_name: {{ params.get('rg_name').get('my_rg') }}
    - parameters:
        chokation: {{ params.get('rg_name').get('your_rg', 'default') }}
        location: {{ params['locations'].get(0) }}
        docation: {{ params['locations'].get(4).get('state').get('city') }}
        mocation: {{ params['locations'].get(3).get('xstate') }}


allowed-locations for {{ params.get('rg_name').get('my_rg') }}:
  azure.policy.policy_assignments.present:
  - require:
    - azure.resource_management.resource_groups: Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}
  - force_update: False
  - scope: /subscriptions/{{ params.get('subscription_id') }}/resourceGroups/{{ params.get('rg_name').get('my_rg') }}
  - policy_assignment_name: allowed_locations
  - parameters:
      properties:
        displayName: Allowed locations for resource group {{ params.get('rg_name').get('my_rg') }}
        enforcementMode: Default
        nonComplianceMessages: []
        notScopes: []
        parameters:
          listOfAllowedLocations:
            value:
            - eastus
            - eastus2
            - westus
            - westus2
        policyDefinitionId: /providers/Microsoft.Authorization/policyDefinitions/e56962a6-4747-49cd-b67b-bf8b01975c4c
