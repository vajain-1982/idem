SLS Parameters
==============

We all love writing Infrastructure as a Code using SLS files. One beauty of
wrinting Infrastructure as a Code is that it should be possible to reuse it
again and again to produce the desired results. Parameters provide us the
ability to customize the same code for different needs.

Creating Parameter File
-----------------------
A parameter file is just another SLS file having key value pairs. Each value
thus specified can be used as parameter in the SLS file using its key. Here
is one sample parameter file:

.. code-block:: sls

        location: eastus
        subscription_id: foo-subscription
        locations:
            - eastus
            - westus


A thing to note is that a parameter file is also another SLS file (barring
a few exceptions). One important exception is that one can not have states
defined in a parameter file. It will be treated as just another property.
But on the flip side it is entirely possible to have `include` statement
inside a parameter file. For e.g.

.. code-block:: sls

        subscription_id: foo-subscription
        include:
            - params_extra


where `params_extra.sls` is another file with the following content:

.. code-block:: sls

        locations:
            - eastus
            - westus


Under the hood the parameters are just Python Dictionary, and all methods
such as get, items, etc. can be freely used to access parameter values
within state declarations.

Writing SLS File to Make Use of Parameter File
----------------------------------------------
Parameters are available in SLS file as Python
`Dictionary <https://docs.python.org/3/library/stdtypes.html#dict>`_
object with name `params`. Since `params` is a dict object one can use
all dict functions such as
`get <https://docs.python.org/3/library/stdtypes.html#dict.get>`_,
`items <https://docs.python.org/3/library/stdtypes.html#dict.items>`_, etc.
Specifically, one can get parameter values like `params.get('parameter')`
where `parameter` is the parameter name. The actual value of the parameter
will be substituted in its place in the SLS file.

The parameters defined in the previous section can be referred to as follows
in the SLS:

* `{{ params.get['location'] }}`
* `{{ params['subscription_id'] }}`
* `{{ params.get['locations'][1] }}`

Executing SLS File Along With Parameters
----------------------------------------
To execute the SLS file along with parameter file, the command line option
`--params` needs to be used. A sample idem command would be:

.. code-block:: bash

        idem idem state --params ~/foo/params.sls --output json ~/foo/bar.sls
