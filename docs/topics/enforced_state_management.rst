=========================
Enforced State Management
=========================

Enforced State Management (ESM) provides idem the ability to track resources across runs.
This makes it possible for resources that aren't natively idempotent to become idempotent through their unique present state name.

In the given context, the previous state (old_state) will be enforced with the following logic:
- Parameters for a resource in the given SLS file will have the highest priority
- If a parameter is not defined in the SLS, it will be pulled from the `old_state` of the previous run
- If there is no `old_state` or the parameter is not in `old_state`, then the default from the python function header will be used.

idem states
-----------

State modules that return "old_state" and "new_state" will have "new_state" available in the ``ctx`` of future runs.

.. code-block:: python

    # my_project_root/my_project/state/my_plugin.py

    __contracts__ = ["resource"]


    def present(hub, ctx, name):
        # ctx.old_state contains the new_state from the previous run
        # When ctx.test is True, there should be no changes to the resource, but old_state and new_state should reflect changes that `would` be made.
        new_state = ...

        return {
            "result": True,
            "comment": "",
            "old_state": ctx.old_state,
            "new_state": new_state,
        }


    def absent(hub, ctx, name):
        # ctx.old_state contains the new_state from the previous run
        return {"result": True, "comment": "", "old_state": ctx.old_state, "new_state": {}}


    def describe(hub, ctx, name):
        ...

context
-------

This feature allows only one instance of an idem state run for a given context.
It also exposes a state dict that can be managed by an arbitrary plugin.
The context is managed for you by idem by idem when you write an esm plugin.
This is how it works and how to use it:

.. code-block:: python

    async def my_func(hub):
        # Retrieve the context manager
        context_manager = hub.idem.managed.context(
            run_name=hub.OPT.idem.run_name,
            cache_dir=hub.OPT.idem.cache_dir,
            esm_plugin="my_esm_plugin",
            esm_profile=hub.OPT.idem.esm_profile,
            acct_file=hub.OPT.acct.acct_file,
            acct_key=hub.OPT.acct.acct_key,
        )

        # Enter the context and lock the run.
        # This calls `hub.esm.my_esm_plugin.enter()` and `hub.esm.my_esm_plugin.get_state()` with the appropriate ctx
        async with context_manager as state:
            # The output of get_state() is now contained in the "state" variable
            # Changes to `state` will persist when we exit the context and `hub.esm.my_esm_plugin.set_state()` is called with the appropriate ctx
            state.update({})
        # After exiting the context, `hub.esm.my_esm_plugin.exit_() is called with the appropriate ctx


Writing a Plugin
----------------

This is the basic format of an ESM plugin:

.. code-block:: python

    # my_project_root/my_project/esm/my_plugin.py
    from typing import Any
    from typing import Dict


    def __init__(hub):
        hub.esm.my_plugin.ACCT = ["my_acct_provider"]


    async def enter(hub, ctx):
        """
        :param hub:
        :param ctx: A dictionary with 3 keys:
            "run_name" the name of the current state run.
            "cache_dir" is the absolute path to idem's execution directory
            "acct" contains the esm_profile from "my_acct_provider"

        Enter the context of the enforced state manager
        Only one instance of a state run will be running for the given context.
        This function enters the context and locks the run.

        The return of this function will be passed by idem to the "handle" parameter of the exit function
        """


    async def exit_(hub, ctx, handle, exception: Exception):
        """
        :param hub:
        :param ctx: A dictionary with 3 keys:
            "run_name" the name of the current state run.
            "cache_dir" is the absolute path to idem's execution directory
            "acct" contains the esm_profile from "my_acct_provider"
        :param handle: The output of the corresponding "enter" function
        :param exception: Any exception that was raised while inside the context manager or None

        Exit the context of the Enforced State Manager
        """


    async def get_state(hub, ctx) -> Dict[str, Any]:
        """
        :param hub:
        :param ctx: A dictionary with 3 keys:
            "run_name" the name of the current state run.
            "cache_dir" is the absolute path to idem's execution directory
            "acct" contains the esm_profile from "my_acct_provider"

        Use the information provided in ctx.acct to retrieve the enforced managed state.
        Return it as a python dictionary.
        """


    async def set_state(hub, ctx, state: Dict[str, Any]):
        """
        :param hub:
        :param ctx: A dictionary with 3 keys:
            "run_name" the name of the current state run.
            "cache_dir" is the absolute path to idem's execution directory
            "acct" contains the esm_profile from "my_acct_provider"

        Use the information provided in ctx.acct to upload/override the enforced managed state with "state".
        """

Extend the esm dyne in your project for a plugin:

.. code-block:: python

    # my_project_root/my_project/conf.py
    DYNE = {"esm": ["esm"]}

refresh
-------

Idem has a new subcommand ``refresh`` which can bring resources from ``describe`` into the Enforced State Management context.

This command:

.. code-block:: bash

    $ idem refresh aws.ec2.*

Is functionally equivalent to these commands:

.. code-block:: bash

    $ idem describe aws.ec2.* --output=yaml > ec2.sls
    $ idem state ec2.sls --test
    $ rm ec2.sls

``idem refresh`` should not make any changes to resources, it only returns the resource attribtues.
(As long as the described resources implement the ctx.test flag properly in their present state).

restore
-------

Enforced State Management keeps a cache of the local run state.
The ``restore`` subcommand will call an ESM plugin's ``set_state`` method with the contents of a json file.

.. code-block:: bash

    $ idem restore esm_cache_file.json

This cache file is generated on every idem state run and is based on idem's ``run_name`` and ``cache_dir``:

.. code-block:: bash

    $ idem state my_state --run-name=my_run_name --cache-dir=/var/cache/idem
    # Cache file for this run will be located in /var/cache/idem/my_run_name.json
