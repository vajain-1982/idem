{% set ctx = hub.idem.ex.ctx("test", acct_profile="default") %}
test_implicit_ctx:
  test.succeed_with_comment:
    - comment: {{ hub.exec.test.ctx(ctx).ret }}

{% set ctx = hub.idem.ex.ctx("exec.test.*", acct_profile="default") %}
test_ctx_minimal:
  test.succeed_with_comment:
    - comment: {{ hub.exec.test.ctx(ctx).ret }}

{% set ctx = hub.idem.ex.ctx("exec.test.*", acct_profile="default") %}
test_ctx_explicit_full_path:
  test.succeed_with_comment:
    - comment: {{ hub.exec.test.ctx(ctx=ctx).ret }}

{% set ctx = hub.idem.ex.ctx("my_cloud", "my_profile") %}
test_minimal_ctx:
  test.succeed_with_comment:
    - comment: {{ hub.exec.test.ctx(ctx).ret }}
