==================================
Secure Multiple Account Management
==================================

You can run Idem against multiple cloud accounts and providers. The Idem `acct` tool lets you specify cloud account and provider information in a file. The `acct` tool is a dependency of Idem. It is used to encrypt the file that stores the account information securely on the file system.

Support for file-based authentication was added as of Idem 6.
Additional authorization mechanisms are expected in future Idem releases.

Static Account Management
=========================

In this example, you create a file in which to store credentials. The file is a simple YAML file that can store credentials for multiple providers and accounts.

The following example `creds.yml` file includes sample aws system values. The only profile shown is an aws default profile, but you could have multiple sections with profiles for more providers and accounts.

.. code-block:: yaml

    aws:
      default:
        aws_access_key_id:
        aws_secret_access_key:
        region_name:

After creating the file with credentials in it, run the `acct` tool to encrypt the file:

.. code-block:: bash

    $ acct encrypt creds.yml
    New encrypted file created at: creds.yml.fernet
    The file was encrypted with this key:
    j-ytfz45n2wRUHDZJsumtG5_Dih3b3lTA1P2apqNuFg=

Now you have an encrypted credentials file and a key to access it. Keep the key in a safe place.

To run `idem` with credentials stored in the file, use the `--acct-file` and `--acct-key` options.

In addition, you can use the `--acct-profile` option to select a profile from within a credentials file that contains multiple profiles. In the example above, `default` is the account profile.

If there are multiple profiles, and you don't supply the `--acct-profile` option, the `default` profile is used.

If you don't want to pass account information as CLI options, you can set the following environment variables:

.. code-block::

    export ACCT_FILE=<full path to creds.yml.fernet>
    export ACCT_KEY=<creds file encryption key>
