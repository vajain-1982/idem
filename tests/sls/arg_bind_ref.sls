first thing:
  test.succeed_with_changes

arg bind ref:
  test.succeed_with_arg_bind:
    - parameters:
        test1: First - ${test:first thing:testing:new}. Second - ${test:first thing:tests[0][0]:new}. Finished
        test2: ${test:first thing:tests[0][0]:new}
        test3:
          - ${test:third thing:testing:new}
          - ${test:third thing:tests[0][0]:new} -- ${test:third thing:tests[0][0]:new}

multi-line ref:
  test.succeed_with_arg_bind:
    - parameters: '{
                       "Id": "key-consolepolicy-3",
                         {
                           "Sid": "Allow access for Key Administrators",
                           "Effect": "Allow",
                           "Principal": {
                             "AWS": [
                               "${test:first thing:tests[0][0]:new}",
                               "${test:first thing:tests[0][0]:new}"
                             ]
                           }'

third thing:
  test.succeed_with_changes

fail reference format:
  test.succeed_with_arg_bind:
    - parameters:
        test1: ${testing:new}

reference within list:
    test.succeed_with_arg_bind:
     - top_list:
        - first_element:
        - second_element:
          inner_list:
            - a: ${test:first thing:tests[0][0]:new}
            - b: ${test:third thing:testing:new}
